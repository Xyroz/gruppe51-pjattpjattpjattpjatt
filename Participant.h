#pragma once
#include "ListTool2B.h"				//  NumElement
#include "Enums.h"					//  Gender

#include <string>					//	String

class Participant : public NumElement //	Class for individual Participants
{	
private:
	std::string name;						//  Name of participant
	Gender gender;							//  Gender of participant
	char* nation;							//  Name of nation
public:
	Participant(int nr);					//	Create with an ID number
	~Participant();							//	Destructor
	Participant::Participant(int nr, std::istream& in);	//	Read from file
	Gender readGender(std::string s);		//	Read Gender
	void display();							//	Display function
	void writeToFile(std::ostream& out);	//	Write data to file
	std::string getName();					//	Returns name
	std::string getNation();				//	Returns Nation
	int getNumber();						//	Returns ID
};