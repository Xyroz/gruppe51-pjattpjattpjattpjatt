#pragma once

class Category;			//	Category
class Exercise;			//	Exercise

class Exercises										//	Class for Exercise functions as well as Result List and Participant List functions
{
public:
	void subMenu();									//	Submenu for various choices
	void printSubMenu(Category* category);			//	Display menu choices
	void subMenuHelp(Category* category);			//	Gives additional info about each menu choice
	Category* categoryExists();						//	Checks if Category exists
	Exercise* exerciseExists(Category* category);	//	Checks if Exercise exists
};