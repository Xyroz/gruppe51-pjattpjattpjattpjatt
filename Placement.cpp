#include "Placement.h"		//
#include "Participants.h"	//	Participants
#include "Functions.h"		//	Functions
#include "Participant.h"	//	Participant
#include "Category.h"		//	Category

#include <iostream>			//	cin, cout

extern Participants* participants;	//	Global class for Participants

Placement::Placement(int res, int part) : NumElement(res) {	//  Constructor, Send res to mother's contructor
	participant = participants->getParticipant(part);   //  Sets participant pointer
}
                    
void Placement::readFromFile(std::ifstream & in) {   //  Function for read from file, File sent as parameter
	int temp;                                           //  Temp int for ID
	in >> number;                                       //  Gets result from file
	in.ignore();                                        //  Ignore '\0'
	in >> temp;                                         //  Gets ID from file
	in.ignore();                                        //  Ignore '\0'
	participant = participants->getParticipant(temp);   //  Sets participant pointer 
}
              
void Placement::writeToFile(std::ofstream & out) {	//  Function for write to file
	out << number << std::endl;                         //  Writes result to file
	out << participant->getNumber() << std::endl;       //  Writes participant ID to file
}

void Placement::display(Category* cat) {	//  Function for displaying result of participant
	std::cout << "\nParticipant number:  " << participant->getNumber() << std::endl;
	std::cout << "Participant name:   " << participant->getName() << std::endl;
	std::cout << "Participant nation:  " << participant->getNation() << std::endl;
	if (cat->getType())									//  If category result is time
	{                              
		displayTime(number, true);		                //	Display time
		std::cout << std::endl;			                //	Add line
	}
    else												//  Else
        std::cout << "\nPoints:  " << number << "\n\n"; //  Display points
}

std::string Placement::getNation() {	//  Function for geting name of nation from participant
	return participant->getNation();				//  Returns name of nation from participant
}