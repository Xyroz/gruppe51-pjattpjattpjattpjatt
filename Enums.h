#pragma once
			
enum Gender	//  Gender enum, Male or Female
{
	Male, Female		//  Male or female
};

enum PointType // PointType enum, Time or Points
{
	Point, Time			//  Time or points
};