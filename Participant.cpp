#include "Participant.h"			//	Participant
#include "Functions.h"				//	Read
#include "Nations.h"				//	Nations
#include "Nation.h"					//	Nation

#include <iostream>					//  cout/cin
#include <string>					//  String
#include <fstream>					//	ifstream, ofstream

extern Nations* nations;

//  Participant class
Participant::Participant(int nr) : NumElement(nr) {		//	Create with an ID number
	number = nr;													//	Set number
	name = readString("Enter Name: ");								//	Read Name
	gender = readGender("Enter Gender (Male/Female): ");			//	Read Gender
	char* chr = readNation();										//	Read Nation abriviation
	nations->nationExists(chr, this);								//	Check if Nation exists, if not create it, adds Participant to Nation
	nation = chr;													//	Set Nation
}

Participant::Participant(int nr, std::istream& in) : NumElement(nr) {		//	Read from file and create with an ID number
	std::getline(in, name);									//	Read Name
	int g;													//	Temporary int for Gender enum
	in >> g; in.ignore();									//	Read Gender
	gender = ((g) ? Female : Male);							//	Set Enum
	std::string temp;										//	Temp string
	std::getline(in, temp);									//	Read Name
	nation = _strdup(temp.c_str());							//	Sets nation to temp string as char*
	if (!nations->nationExists(nation, this))				//	Adds self to Nation. ALWAYS READ NATIONS FROM FILE FIRST!
		std::cout << "ERROR! Nation was not found while reading from file, make sure to read Nations from file first!" << std::endl;
}

Participant::~Participant() {				//	Destructor
	Nation* temp = nations->getNation(nation);	//	Finds Nation
	temp->removeParticipant(number);			//	Removes self from Nation
	delete[] nation;
}

Gender Participant::readGender(std::string s) {	//	Read Gender
	Gender gender;									//	Temp Gender
	char g;											//	Temp char for Gender
	do {											//	Loop until valid
		g = read(s);								//	Read Gender to char
	} while (g != 'M' && g != 'F');					//	Loop if char isn't M or F
	if (g == 'M') gender = Male;					//	If M, set as Male
	if (g == 'F') gender = Female;					//	If F, set as Female
	return gender;									//	Return Gender
}

void Participant::display() {						//	Displays info about the participant
	std::cout << "ID number: " << number << std::endl;								//	Display ID number
	std::cout << "Name: " << name << std::endl;										//	Display name
	std::cout << "Gender: " << ((gender == Male) ? "Male" : "Female") << std::endl;	//	Display gender
	std::cout << "Nation: " << nation << std::endl;									//	Display nation
	std::cout << std::endl;
}

void Participant::writeToFile(std::ostream& out) {	//	Write data to file
	out << number << std::endl;							//	Write ID number
	out << name << std::endl;							//	Write Name
	out << gender << std::endl;							//	Write Gender
	out << nation << std::endl;							//	Write Nation
}

std::string Participant::getName() {	//	Returns the name of the Participant
	return name;
}

std::string Participant::getNation() {	//	Returns the 3-letter abriviation for the Nation of the Participant
	return nation;
}

int Participant::getNumber() {			//	Returns the ID number of the Participant
	return number;
}
