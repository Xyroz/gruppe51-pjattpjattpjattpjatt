#pragma once
const int MAXID = 99999;	            //	Global const for max ID allowed
const int MAXTEXT = 80;		            //  Max text lenght
const int MINNUMBER = 1;                //  Lowest number allowed
const int MAXNUMBER = 10000;            //  Highest number allowed
const int MINPARTICIPANTS = 2;          //  Minimum number of participants
const int MAXPARTICIPANTS = 200;        //  Maximum number of participants
const int MAXCATEGORIES = 100;			//	Maximum number of categories
const int MAXEXERCISES = 20;            //  Maximum number of exercises
const int MINPOINTS = 0;                //  Minimum score for exercise
const int MAXPOINTS = 1000;             //  Maximum score for exercise
const int MINPHONE = 10000000;          //  First phonenumber
const int MAXPHONE = 999999999;         //  Last phonenumber
const int MAXNATIONS = 200;             //  Maximum number of nations

const int GOLD[3] = { 1, 0, 0 };        //  Const array for gold medal
const int SILVER[3] = { 0, 1, 0 };      //  Const array for silver medal
const int BRONZE[3] = { 0, 0, 1 };      //  Const array for bronze medal

const int FIRST = 7;                    //  Points for first place
const int SECOND = 5;                   //  Points for second place
const int THIRD = 4;                    //  Points for third place
const int FOURTH = 3;                   //  Points for fourth place
const int FIFTH = 2;                    //  Points for fifth place
const int SIXTH = 1;                    //  Points for sixth place