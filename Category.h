#pragma once
#include "ListTool2B.h"	//	Lists
#include "Enums.h"		//	PointType

#include <string>		//	String

class Exercise;			//	Exercise

class Category : public TextElement	//	Class for individual Categories
{
private:
	PointType pointType;		//  Enum for points
	List* exercises;			//  List with Exercise objects
	int numExercises;			//  Int for amount of exercises used
public:
	Category(char* name);		//  Constructor
	Category(std::ifstream& in, char* name);	//	Read from file
	void edit();				//  Edit name
	void display();				//  Display main data
	void displayAll();			//  Display main data and exercises
	void newExercise();			//	Adds a new exercise if possible
	void editExercise();		//	Edit exercise
	void deleteExercise();		//	Delete exercise
	void displayExercises();	//	Display exercise
	bool getType();				//	Return type
	Exercise* findExercise(std::string name);	// Find and return an exercise pointer
	char* getName();			//	Return name
	void writeToFile(std::ofstream& out);	//	Write all data to file
};