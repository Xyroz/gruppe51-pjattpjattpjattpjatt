#include "Categories.h"						//	
#include "Category.h"						//  Category class
#include "Const.h"							//	Consts
#include "Globals.h"						//  Command var
#include "Functions.h"						//  Read functions

#include <iostream>							//  cin, cout
#include <fstream>							//	Read/Write File

Categories::Categories() {	//  Constructor
	categories = new List(Sorted);		//  Makes a new sorted list
}
	
void Categories::printMenu() {	//  Function to display menu choices
	std::cout << "\nSUBMENU - CATEGORIES"
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - New Category"
		<< "\n\tE - Edit Category"
		<< "\n\tA - Display all Categories"
		<< "\n\tD - Display a single Category"
		<< "\n\tH - Help"
		<< "\n\tB - Back to Main Menu\n";
}

void Categories::subMenuHelp() {	//	Gives additional info about each menu choice
	std::cout << "\nHELP - CATEGORIES"
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - Creates a new Category with various data"
		<< "\n\tE - Edits an existing category"
		<< "\n\tA - Displays all categories"
		<< "\n\tD - Displays a single category"
		<< "\n\tB - Back to Main Menu\n";
}

void Categories::subMenu() {	//  Function for menu
	printMenu();								//  Displays menu choices
	command = read("\nCommand (Categories):  ");//  Takes a command
	while (command != 'B')						//  Loop while not 'B' for back
	{
		switch (command)						//  Switch for command
		{
		case 'N':	addNewCategory();	break;	//  Case for adding new category
		case 'E':	editCategory();		break;	//  Case for editing category
		case 'A':	display();			break;	//  Case for displaying all categories
		case 'D':	displayCategory();	break;	//  Case for displaying a category
		case 'H':	subMenuHelp();		break;	//	Gives additional info about each menu choice
		default:	printMenu();		break;	//  Default, display menu choices
		}
		command = read("\nCommand (Categories):  ");	//  Takes a command
	}
	mainMenu();							//	Display main menu when quitting
}

void Categories::addNewCategory() {		//  Function for adding a new category to list
	if (categories->noOfElements() < MAXCATEGORIES)
	{
		std::string temp = readString("Enter name of the category:   ");
		//  Reads name of category 
		if (!categories->inList(temp.c_str()))		//  If name doesn't exist in list
		{
			Category* category;						//  Temp category pointer
			category = new Category(_strdup(temp.c_str()));	//  Makes a new category
			categories->add(category);				//  Adds category to list
			writeToFile();							//	Write changes to file
		}
		else										//	if Category already exists
			std::cout << "Category '" << temp << "' already exists!" << std::endl;	//	Error Message
	}
	else
		std::cout << "There's no more room for more Categories!" << std::endl;			//	Write error message
}

void Categories::editCategory() {		//  Function for changing a category
	if (!categories->isEmpty())
	{
		std::string temp = readString("Enter name of the category to change:   ");
		//  Reads name of category 
		if (categories->inList(temp.c_str()))		//  If name exist in list
		{
			Category* category;						//  Temp category pointer
			category = (Category*)categories->remove(temp.c_str());
			//  Gets category from list
			category->edit();						//  Changes the category
			categories->add(category);				//  Adds category back in list
			writeToFile();							//	Write changes to file
		}
		else 										//  if Category doesn't exist
			std::cout << "Category '" << temp << "' doesn't exist!" << std::endl;	//	Error Message
	}
	else
		std::cout << "There are no registered Categories!" << std::endl;	//	Write error message
}
	
void Categories::display() {	//  Function for displaying all categories
	if (!categories->isEmpty())		
		categories->displayList();	//	Display all Categories in list
	else							//	if there are no Cateogires
		std::cout << "There are no registered Categories!" << std::endl;	//	Error Message
}

void Categories::displayCategory() {	//  Function for displaying a category
	if (!categories->isEmpty())
	{
		std::string temp = readString("Enter name of the category:   ");
		//  Reads name of category 
		if (categories->inList(temp.c_str()))		//  If name exist in list
		{
			Category* category; 					//  Temp category pointer
			category = (Category*)categories->remove(temp.c_str());
			//  Gets category from list
			category->displayAll();					//  Displays all data from category
			categories->add(category);				//  Adds category back in list
		}
		else 										//  if Category doesn't exist
			std::cout << "Category '" << temp << "' doesn't exist!" << std::endl;	//	Error Message
	}
	else
		std::cout << "There are no registered Categories!" << std::endl;	//	Write error message
}

Category* Categories::findCategory(std::string name)	//	Finds and returns a pointer to a Category
{
	Category* category = nullptr;				//  Temp category pointer
	if (categories->inList(name.c_str()))		//  If name exist in list
	{
		category = (Category*)categories->remove(name.c_str());	//	Remove from list and set pointer
		categories->add(category);				//	Add back into list
	}
	return category;							//	Return pointer
}

void Categories::writeToFile() {							//	Write all data to file
	std::ofstream outFile("Categories.dta");					//	Output file
	if (outFile)
	{
		Category* temp;										//	Temp pointer
		for (int i = 1; i <= categories->noOfElements(); i++)	//	For each participant
		{
			temp = (Category*)categories->removeNo(i);			//	Remove from list and set temp
			categories->add(temp);								//	Add back into list
			temp->writeToFile(outFile);							//	Write data to file
		}
	}
}

void Categories::readFromFile() {				//	Read all data from file
	std::ifstream inFile("Categories.dta");			//	Input file
	if (inFile)											//	if file exists
	{
		while (!inFile.eof())							//	As long as the program hasn't reached end of file
		{
			char name[MAXTEXT];							//	Category name
			inFile >> name; inFile.ignore();
			if (!inFile.good())							//	If read data wasn't valid
				break;									//	Stop reading
			Category* c = new Category(inFile, name);	//	Create new Category from file
			categories->add(c);							//	Add to list
		}
	}
	else												//	if file doesn't exist
		std::cout << "Couldn't find 'Categories.dta'!" << std::endl;	//	Error message
}