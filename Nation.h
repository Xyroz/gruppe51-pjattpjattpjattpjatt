#pragma once
#include "ListTool2B.h"				//  List and TextElement

#include <string>					//	String

class Participant;

class Nation : public TextElement	//  Class for individual Nations
{
private:
	std::string name;				//  Name of nation
	std::string contactPerson;		//  Name of contact person
	int phoneNumber;				//  Phone number of contact person

	List* participants;				// list of type sorted

public:
	Nation(char*t);							//	Constructor with text element
	Nation(std::ifstream& in, char* t);		//	Read from file with text element
	void edit();							//	Edit data in class
	void display();							//	Display function for the nation
	void displayParticipants();				//	Display funtion for the participants 
	void addParticipant(Participant* p);	//	Add new participants to the nation
	void removeParticipant(int id);			//	Removes a Participant from Nation based on ID
	std::string getName();					//	Returns name
	void displayAll();						//	Display all info
	void writeToFile(std::ofstream& out);	//	Write all data to file
	char* getText();						//	Returns the 3-letter abriviation of the Nation
};