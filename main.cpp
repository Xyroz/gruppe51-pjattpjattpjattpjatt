//#include
#include "Functions.h"			//  Non-class functions
#include "Globals.h"			//  Globals
#include "Categories.h"			//	Categories
#include "Participants.h"		//	Participants
#include "Nations.h"			//	Nations
#include "Medals.h"				//	Medals
#include "Points.h"				//	Points
#include "Exercises.h"			//	Exercises

char command;
Categories* categories = new Categories();			//	Creates Categories Class
Participants* participants = new Participants();	//	Creates Participants Class
Nations* nations = new Nations();					//	Creates Nations Class
Medals* medals = new Medals();						//	Creates Medals Class
Points* points = new Points();						//	Creates Points Class
Exercises* exercises = new Exercises();				//	Creates Exercises Class

int main()				//  Main program
{
	nations->readFromFile();		//	Read data from file
	participants->readFromFile();	//	Read data from file
	categories->readFromFile();		//	Read data from file
	medals->readFromFile();			//	Read data from file
	points->readFromFile();			//	Read data from file

	mainMenu();						//  Shows main menu
	command = read("\nCommand:   ");//  User input

	while (command != 'Q')			//  While not 'Q' for Quit
	{
		switch (command)			//  Main switch
		{
		case 'N':	nations->subMenu();		break;	//  Case for nations
		case 'P':	participants->subMenu();break;	//  Case for participants
		case 'C':	categories->subMenu();	break;	//  Case for categories
		case 'E':	exercises->subMenu();	break;	//  Case for exercises
		case 'M':	medals->display();		break;	//  Case for medals
		case 'S':	points->display();		break;	//  Case for score/points
		case 'H':	menuHelp();				break;	//	Gives additional info about each menu choice
		default:	mainMenu();				break;	//  Default when none of the above
		}
	command = read("\nCommand:   ");	//  User input
	}

	nations->writeToFile();			//	Write data on program quit
	participants->writeToFile();	//	Write data on program quit
	categories->writeToFile();		//	Write data on program quit
	medals->writeToFile();			//	Write data on program quit
	points->writeToFile();			//	Write data on program quit

	return 0;						//  End programs
}

