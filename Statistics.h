#pragma once
#include <vector>	//	Vector

class Nation;		//	Nation

class Statistics	//	Base class for Medals and Points
{
protected:
	std::vector<char*> abriviation;         //  3 letter name for countries
	int numberOfNations;                    //  Number of nations
	std::vector<Nation*> statNation;        //  Vector with pointers to nations
public:
	Statistics();                           //  Constructor
};