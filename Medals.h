#pragma once
#include "Statistics.h"

class Medals : public Statistics            //  Child of Statistics
{
private:
	int gold[200];                          //  Array for gold medals
	int silver[200];                        //  Array for silver medals
	int bronze[200];                        //  Array for bronze medals
public:
    Medals();                               //  Constructor
	void display();                         //  Display function
	void addMedals(const int m[3], std::string name);   //  Add medals function
    void removeMedals(const int m[3], std::string name);//  Remove medals function
	void sort();                            //  Sort function
	void addNation(int i, std::string name);//  Add nation function
	void writeToFile();						//	Writes all data to file
	void readFromFile();					//	Reads all data from file
};