#pragma once
#include "ListTool2B.h"	//	List

#include <fstream>		//	Read/Write file

class Category;			//	Class for an individual Category

class Exercise : public TextElement	//	Class for an individual Exercise
{
private:
	int date;						//	Date of the exercise
	int time;						//  Time of day exercise runs
public:
	Exercise(char* name);			//	Constructor
	Exercise(std::ifstream& in, char* name);	//	Constructor, read from file
    ~Exercise();                    //  Destructor
	void display();					//  Function for displaying data
	void edit();					//	Edit Exercise
	char* getName();				//	Returns name
	void writeToFile(std::ofstream& out);	//	Write all data to file
};