#include "Points.h"				//	Points
#include "Nations.h"			//	Nations
#include "Nation.h"				//	Nation

#include <iostream>				//  cin, cout
#include <fstream>				//	Read/Write File
#include <string>				//	String

extern Nations* nations;		//	Global class for Nations

                   
void Points::display() { //  Function for displaying points
	if (numberOfNations > 0)
	{
		std::cout << "\nPOINTS:\n";
		for (int i = 0; i < numberOfNations; i++)       //  Goes through everty nation
		{                                               //  Displays name and points
			std::cout << "\t" << statNation[i]->getName() << std::endl;
			std::cout << "\t\tPoints: " << points[i] << "\n\n";
		}
	}
	else
		std::cout << "There are currently no registered Nations with any Points!" << std::endl;
}
                
void Points::addPoints(int p, std::string name) {   //  Function for adding points to array, Points and name of nation as parameters
    bool added = false;                             //  Bool if added points
	int i = 0;                                      //  Counting int

	for (; i < numberOfNations; i++)				//  Goes through all nations currently in array
	{
		if (statNation[i]->getText() == name)		//  If nation's name is same as parameter
		{
			points[i] += p;                         //  Adds points
			added = true;                           //  Sets added to true
		}
	}

	if (!added)										//  If not added
	{
		addNation(i, name);                         // Adds new nation to arrays
		points[i] += p;                             //  Adds points to that nation
	}
	sort();                                         //  Sorts the arrays
	writeToFile();									//	Write changes to file
}

void Points::removePoints(int p, std::string name) {//  Function for removing points from array, points and anme of nation as parameters

    for (int i = 0; i < numberOfNations; i++)		//  Goes through all nations currently in array
    {
        if (statNation[i]->getText() == name)		//  If nation's name is same as parameter
        {
            points[i] -= p;                         // Removes points
        }
    }
}
           
void Points::addNation(int i, std::string name) {	//  Function for adding a new nation to the arrays
	statNation.push_back(nations->getNation(name)); //  Adds a new nation in the vector
	abriviation[i] = statNation[i]->getText();      //  Sets abriviation to nation's abriviation
	numberOfNations++;                              //  Increases number of nations
}

void Points::sort() {	//  Function for sorting arrays
	int temp = 0;                                   //  Temp int for points
	char* tempName = nullptr;                       //  Temp name pointer
	Nation* tempNation = nullptr;                   //  Temp nation pointer
	
	for (int i = 0; i < numberOfNations; i++)		//  Goes through all nations
	{
		if (points[i] < points[i + 1])				//  If points is less than the next
		{
			temp = points[i];                       //  Sets temp points
			tempName = abriviation[i];              //  Sets temp name
			tempNation = statNation[i];             //  Sets temp nation
			
			points[i] = points[i + 1];              //  Gets points from next in array
			abriviation[i] = abriviation[i + 1];    //  Gets name from next in array
			statNation[i] = statNation[i + 1];      //  Gets nation from next in array

			points[i + 1] = temp;                   //  Sets points of next in array
			abriviation[i + 1] = tempName;          //  Sets name of next in array
			statNation[i + 1] = tempNation;         //  Sets nation of next in array
		}
	}
}

void Points::writeToFile() {						//	Write all data to file
	std::ofstream outFile("Points.dta");						//	Output file
	if (outFile)
	{
		outFile << numberOfNations << std::endl;				//	Write number of nations to file
		for (int i = 0; i < numberOfNations; i++)				//	For each nation
		{
			outFile << statNation[i]->getText() << std::endl;	//	Write name to file
			outFile << points[i] << std::endl;					//	Write points to file
		}
	}
}

void Points::readFromFile() {						//	Read all data from file
	std::ifstream inFile("Points.dta");						//	Input file
	if (inFile)												//	if file exists
	{
		int nrNations;										//	Temp int for number of Nations
		inFile >> nrNations;								//	Read from file
		for (int i = 0; i < nrNations; i++)					//	For each nation
		{
			std::string nation;								//	Nation name
			inFile >> nation;								//	Read name from file
			addNation(i, nation);							//	Set pointer to Nation
			inFile >> points[i];							//	Read points from file
		}
	}
	else													//	if file doesn't exist
		std::cout << "Couldn't find 'Points.dta'!" << std::endl;	//	Error message
}