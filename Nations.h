#pragma once
#include "ListTool2B.h"	//	List

#include <string>		//	String

class Nation;			//	Nation
class Participant;		//	Participant

class Nations			//	Class containing a List of Nations as well as various functions
{
private:
	List* nations;			//	List of all Nations

public:
	Nations();				//	Constructor
	void printMenu();		//  Outputs available commands to window
	void subMenu();			//	Submenu for choosing options
	void subMenuHelp();		//	Gives additional info about each menu choice
	Nation* newNation();	//	Creates a new Nation and returns a pointer
	Nation* newNation(char*);	//	Creates a new Nation with pre-determined text
	void editNation();		//	Edit a Nation
	void displayNations();	//	Run the display function for all Nations
	void displayParticipants();	// Display a given nations participants
	void display();			//	Display info about a nation as well as participants
	bool nationExists(char* chr, Participant* p);	//	Participant checks if Nation exists and either adds itself to Nation or creates a new Nation and then adds itself
	void writeToFile();		//	Write all data to file
	void readFromFile();	//	Read all data from file
	Nation* getNation(std::string name);	//	Fetches nation and returns it as a pointer
};