#pragma once
#include "Statistics.h"

class Points : public Statistics                    //  Child of Statistics
{
private:
	int points[200];                                //  Array of points

public: 
	void display();                                 //  Display function
	void addPoints(int p, std::string name);        //  Add points function
    void removePoints(int p, std::string name);     //  Remove points function
	void sort();                                    //  Sort function
	void addNation(int i, std::string name);        //  Add nation function
	void writeToFile();								//	Writes all data to file
	void readFromFile();							//	Reads all data from file
};