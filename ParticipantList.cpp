#include "ParticipantList.h"	//
#include "Const.h"				//	Consts
#include "Exercises.h"			//	Exercises
#include "Exercise.h"			//	Exercise
#include "Functions.h"			//	Standard Functions
#include "Globals.h"			//	Globals
#include "Category.h"			//	Category
#include "Participant.h"		//	Participant
#include "Participants.h"		//	Participants

#include <fstream>				//	Read/Write File
#include <stdio.h>				//	
#include <vector>				//	Vector
#include <iostream>				//	cin, cout

extern Participants* participants;		//	The global class for Participants

void ParticipantList::subMenu(Category* category, Exercise* exercise) {	//	Submenu with various functions regarding Participants in Exercises
	printSubMenu(exercise->getName());						//	Display menu
	command = read("\nCommand (Participant list):   ");		//  User input

	while (command != 'B')									//  While not 'B' for Back
	{
		switch (command)									//  Main switch
		{
		case 'D':	displayList(exercise->getName());	break;	//  Display Participant List
		case 'N':	newList(exercise->getName());		break;	//  Create new Participant List
		case 'E':	editList(exercise->getName());		break;	//  Edit current Participant List
		case 'R':	removeList(exercise->getName());	break;	//  Delete the current Participant List
		case 'H':	subMenuHelp(exercise->getName());	break;	//  Display help for menu choices
		default:	printSubMenu(exercise->getName());	break;	//  Default when none of the above, shows menu
		}
		command = read("\nCommand (Participant list):   ");	//  User input
	}
}

void ParticipantList::printSubMenu(std::string s) {	//	Prints the various options for the submenu
	std::cout << "\nSUBMENU - PARTICIPANTLIST, " << s << ": "
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:" 
		<< "\n\tD - Display Participant List"
		<< "\n\tN - New Participant List"
		<< "\n\tE - Edit Participant List"
		<< "\n\tR - Remove Participant List"
		<< "\n\tH - Help"
		<< "\n\tB - Back to previous sub-menu\n";
}

void ParticipantList::subMenuHelp(std::string s) {	//	Gives a detailed explanation of each function in the submenu
	std::cout << "\nHELP - PARTICIPANTLIST, " << s << ": "
		<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tD - Display participant list for Exercise " << s
		<< "\n\tN - Create a new participant list for Exercise " << s
		<< "\n\tE - Edit existing participant list for Exercise " << s
		<< "\n\tR - Remove existing participant list for Exercise " << s
		<< "\n\tB - Back to previous sub-menu\n";
}

void ParticipantList::newList(std::string s) {	// COMMAND 'N'  create new participant list
	std::string filename = ("EX" + s + ".sta"); //	The file name of the participant resualt list. where "EX" denotes that this file belongs to exersice "S"
	std::ifstream in(filename);				//	creates a ifstream object and attempt to open a file.

	if (!in)			//	checks if File with the exercise name exists
	{
		if (participants->getNrOfPart() >= MINPARTICIPANTS)
		{
			int noOfParticipants;		//	int for the amount the partcipants in the list.
			int participantNumber;		//	The participants number
			int PartAlreadyInList[MAXPARTICIPANTS];	//	contains all participant numbers that have been added
			std::string partName;		//	the participants name
			std::string partNation;		//	the participants nation
			bool addPart;				//	bool for wether the participant should be added or not.

			std::ofstream out(filename);   // creates out stream

			noOfParticipants = read("How many participants are there: ", MINPARTICIPANTS, participants->getNrOfPart());
			out << noOfParticipants << std::endl;	//	writes number of participatns at the top of file
			for (int i = 0; i < noOfParticipants;)  //	goes through the amount given.
			{
				addPart = true;						//	sets addPart to true so that if the participant is not in the list he will get added
				participantNumber = read("Add participant number: ", MINNUMBER, MAXNUMBER); //	gets participaiton number for participant
				Participant* temp = participants->getParticipant(participantNumber);		//	fetches the given participant and sets temp to point to it
				if (temp != nullptr)									//	Checks that temp actually points to an participant
				{
					for (int i = 0; i < noOfParticipants; i++)
						if (participantNumber == PartAlreadyInList[i])		//	checks if participant has already been added
							addPart = false;								//	if it has, set bool to false.

					if (addPart)							//	checks if participant should be added or not.
					{
						i++;						//	increments i so a new participant can be added.
						PartAlreadyInList[i] = participantNumber;
						out << i << std::endl;						//	participant start number;
						out << participantNumber << std::endl;		//	writes to file the participant number
						partName = temp->getName();					//	fetches the partcipants name
						out << partName << std::endl;				//	 and writes it to file
						partNation = temp->getNation();				//	fetches the participants nation
						out << partNation << std::endl;				//	 and writes it to file
					}
					else					//	if the partcipant is already in the list, write this message
						std::cout << "\tPartcicipant already in list" << std::endl;
				}
				else						//	if the participant does not exist, write this message
					std::cout << "\tParticipant does not exists" << std::endl;
			}
		}
		else								
			std::cout << "There are currently too few Participants to make a list, need " << MINPARTICIPANTS << std::endl;
	}	
	else								//	if the participant list already exist, write this message
		std::cout << "Participant list already exists" << std::endl;
}

void ParticipantList::displayList(std::string s) {  //	Display all info in List
	std::string filename = ("EX" + s +".sta");	
	std::ifstream in(filename); //	Opens a ifstream object

	if (in)   //	checks if a file by the given name exists
	{
		int noOfParticipants;	//	int for the amount the partcipants in the list.
		int startNumber;		//	The participants start number
		int participantNumber;	//	The participants number
		std::string partName;	//	the participants name
		std::string partNation;	//	the participants nation

		in >> noOfParticipants;						//	how many participatns this list has
		for (int i = 0; i < noOfParticipants; i++)
		{
			in >> startNumber;						//	fetches start number
			in >> participantNumber; in.ignore();	//	fetches participant number
			in >> partName;							//	fetches participant name
			in >> partNation;						//	fetches partcipant nation

			std::cout << "\nStart number: " << startNumber			//	writes out all the values it just fetched
				<< "\nParticipant number: " << participantNumber
				<< "\nParticipant Name:  " << partName
				<< "\nParticipants Nation: " << partNation << std::endl;
		}
	}
	else
		std::cout << "Could not find file '" << filename << "'" << std::endl; //	if the file does not exist.
}

void ParticipantList::removeList(std::string s)	{	//	COMMAND 'R'	removes a given file
	std::string fileName = "EX" + s + ".res";		//	tries to open a resualt list
	std::ifstream in(fileName.c_str());
	if (!in)									//	if it does not exist
	{
		std::string file = "EX" + s + ".sta";	//	open the sta file
		if (remove(file.c_str()) == 0)			//	if the function returns 0, the file has been deleted
			std::cout << "File has been deleted!" << std::endl;
		else		//	if not, file does not exist
			std::cout << "Error! file could not be found!" << std::endl;	
	}
	else			//	if a .res file exists
		std::cout << "Error! Results are already in, you are not allowed to change this file!" << std::endl;
}

void ParticipantList::editList(std::string s) {		//	lets the user edit a given file's participants
	std::string fileName = "EX" + s + ".res";
	std::ifstream in(fileName.c_str());		//	checks if the resualts are already in.

	if (!in)
	{
		std::ifstream inn("EX" + s + ".sta");	//	checks if a given STA file actually exists for editing
		if (inn)
		{
			int noOfParticipants;
			std::vector<int> startNumber;				//  Vector for start numbers
			std::vector<int> participantNumber;			//  Vector for participant numbers
			std::vector<std::string> partName;			//  Vector for participant name
			std::vector<std::string> partNation;		//  Vector for participant nation
			char cont;

			inn >> noOfParticipants;				//	fetches the amount of participants from the file

			startNumber.resize(noOfParticipants);			//  Resize all vectors
			participantNumber.resize(noOfParticipants);		//  to number of
			partName.resize(noOfParticipants);				//  participants
			partNation.resize(noOfParticipants);


			for (int i = 0; i < noOfParticipants; i++)		//	loops for all participants on the file
			{
				inn >> startNumber[i];						//	fetches the participants start number
				inn >> participantNumber[i]; inn.ignore();	//	fetches the participants number
				inn >> partName[i];							//	fetches the participants name
				inn >> partNation[i];						//	fetches the participants nation

				std::cout << "\nStart number: " << startNumber[i]		//	displays all the participants as they are read in
					<< "\nParticipant number: " << participantNumber[i]
					<< "\nParticipant Name:  " << partName[i]
					<< "\nParticipants Nation: " << partNation[i] << std::endl;
			}
			
			inn.close();	//	closes "inn" after displaying all participants
			do
			{				 
				int command;			//	int for the particiapants number
				int index;				//	index for where in the vector the given participant is
				command = read("Write the participation number of the participant you would like to remove: ", MINNUMBER, MAXNUMBER);
				for (int i = 0; i < noOfParticipants; i++)
				{
					if (command == participantNumber[i])				//	checks if the participant number given matches with the file
					{
						index = i;										//	if it does, the vector index is set to that number
					}
				}
				Participant* temp = nullptr;
				while (temp == nullptr)
				{
					participantNumber[index] = read(" New participant number: ", MINNUMBER, MAXNUMBER);	//	gets new participaiton number for participant
					temp = participants->getParticipant(participantNumber[index]);	//	pointer points to the returned object	

					if (temp != nullptr)
					{															 //	if the returned pointer is not a nullpointer, fetche that participants's info
						partName[index] = temp->getName();
						partNation[index] = temp->getNation();
					}
					else														//	if is a nullpointer, a error is displayed
						std::cout << "Participant does not exist!" << std::endl;
				}
				cont = read("Would you like to edit more participants? [Y/N]:  ");		//	if user wants to countinue editing, it restarts the function.
			} while (cont != 'N');

			std::ofstream out("EX" + s + ".sta", std::ofstream::trunc);				//	open a ofstream object that truncates any previous file
			out << noOfParticipants << std::endl;					//	write out the number of participants at the top of the file.
			for (int i = 0; i < noOfParticipants; i++)						//	writes all the participants to file
			{
				
				out << startNumber[i] << std::endl;				//	writes out, the start number, participant number, name and natiin for all participants in the vector.
				out << participantNumber[i] << std::endl;
				out << partName[i] << std::endl;
				out << partNation[i] << std::endl;
			}
		}
		else
			std::cout << "Could not find .sta file!" << std::endl; //	if the file does not exist.
	}
	else			//	If a .res file already exits an error is writen on the screen.
		std::cout << "Error! Results are already in, you are not allowed to change this file!" << std::endl;
}