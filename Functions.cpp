#include "Functions.h"			//	
#include "Globals.h"			//  char command

#include <iostream>				//  cin, cout
#include <cctype>				//  toupper


//-----------------------------------------------------------------------------------
//  User input
//-----------------------------------------------------------------------------------

//	Read string
std::string readString(std::string s) {	//	Read text string
	std::string temp;							//	String
	do {
		std::cout << "\t" << s;						//	Write text
		std::getline(std::cin, temp);				//	Read string
	} while (temp.length() == 0);
	temp = autoCapitalize(temp);				//	Auto capitalize and remove spaces
	return temp;								//	Return string
}

//	Read Character
char read(std::string s) {				//	Reads and returns one upcast character
	std::string temp = readString(s);
	return (toupper(temp.at(0)));				//	Set to uppercase and return
}

//	Read Integer
int read(std::string s, int min, int max) {	//	Reads and returns an int
	int n;										//	Number
	do {
		std::cout << '\t' << s << " (" << min << '-' << max << "):  ";	//	Write text
		if (!std::cin)							//	if valid input was entered
		{
			std::cin.clear();					//	Clear input
			std::cin.ignore();					//	Ignore next line
		}
		std::cin >> n; std::cin.ignore();		//	Read input
	} while (!std::cin || n < min || n > max);	//	if number is valid
	return n;									//	Return number
}

//	Read Date as Integer
int readDate(std::string s) {				//	Read date as int
	std::cout << s << std::endl;
	int date = 0;								//	Entered date
	int year = read("Year", 2000, 2999);		//	Year
	date += year*100*100;						//	Add year to total date
	int month = read("Month", 01, 12);			//	Month
	date += month*100;							//	Add month to total date
	int day = 31;								//	Jan, Mar, Jul, Aug, Oct, Dec
	if (month == 4 || month == 6 || month == 9 || month == 11)
		day = 30;								//	Apr, Jun, Sep, Nov
	if (month == 2)
		day = (year % 4 == 0)? 29 : 28;			//	Feb, with simplified leap year calculation
	date += (read("Day", 01, day));				//	Day
	return date;								//	Return number
}

//	Read Time as Integer
int readTime(std::string s, bool scoring) {				//	Read time as int
	std::cout << s << std::endl;
	int time = 0;												//	Entered time
	time += read("Hours", 00, (scoring)?99:23) *100 *100 *1000;	//	Hours
	time += read("Minutes", 00, 59) *100 *1000;					//	Minutes
	if (scoring)												//	if time is for score-keeping
	{
		time += read("Seconds", 00, 59) * 1000;					//	Seconds												
		time += read("Thousandths", 000, 999);					//	Thousandths
	}
	return time;												//	Return number
}

//	Read Nation as char*
char* readNation() {								//	Read and return a 3 character Nation abriviation
	std::string temp;												//	Temp string
	do {															//	Loop until valid string length
		temp = readString("Enter 3 letter abriviaton for the nation:  ");	//	Read string
		if (temp.length() != 3)												//	Check length
			std::cout << "Entered abriviation '" << temp << "' is not 3 characters long!" << std::endl;	//	Error message
	} while (temp.length() != 3);									//	Loop while length isn't 3
	char* chr = _strdup(temp.c_str());								//	Cast temp to char*
	for (int i = 0; i < 3; i++)										//	For each char
		chr[i] = toupper(chr[i]);									//	Set to upper
	return chr;														//	Return char*
}

std::string autoCapitalize(std::string s) {	//	Auto Capitalizes and removes spaces
	char* temp = _strdup(s.c_str());						//	Creates a char* from string
	int length = s.length();								//	Find length of string
	if (length > 0)											//	if length over 0
	{
		for (int i = 0; i < length; i++)					//	Loop through string
		{
			temp[i] = tolower(temp[i]);						//	Set each character to lower case
				while (temp[i] == ' ' && i != length) 
				{										//	Loop until no more spaces are found
					temp[i] = toupper(temp[i + 1]);			//	Capitalize next character
					for (int j = i + 1; j < length - 1; j++)//	Loop through all remaining characters
					{
						temp[j] = tolower(temp[j + 1]);		//	Move characters down
					}
					length--;								//	Reduce length
				} 	//	Contine looping if there are more spaces, stop if reached end of string
			if (i == 0)										//	if first character
				temp[i] = toupper(temp[i]);					//	Capitalize first character
			if (i+1 == length)								//	if last character
				temp[i+1] = '\0';							//	Set to \0
		}
	}
	else
		temp[0] = '\0';
	std::string string(temp);					//	Create string
	delete[] temp;								//	Delete char*
	return string;								//	Return string
}

//-----------------------------------------------------------------------------------
//  Display
//-----------------------------------------------------------------------------------

//  Main menu
void mainMenu()	{		//  Outputs available commands to window			
	std::cout << "\nMAIN MENU"
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - Nations"
		<< "\n\tP - Participants"
		<< "\n\tC - Categories"
		<< "\n\tE - Exercises"
		<< "\n\tM - Medals"
		<< "\n\tS - Score"
		<< "\n\tH - Help"
		<< "\n\tQ - Quit\n";
}

void menuHelp()	{		//	Gives additional info about each menu choice
	std::cout << "\nHELP"
		<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - Allows you to add, edit, and display info about Nations"
		<< "\n\tP - Allows you to add, edit, and display info about Participants"
		<< "\n\tC - Allows you to add, edit, and display info about Categories"
		<< "\n\tE - Allows you to add, edit, display, and delete info about Exercises, as well as Participant and Result Lists"
		<< "\n\tM - Prints all info from Medals"
		<< "\n\tS - Prints all info from Score"
		<< "\n\tQ - Quits the program!\n";
}

void displayTime(int time, bool scoring) {		//	Displays formatted time
	std::cout << "Time: " 						//	Display Time
		<< (((time / (100 * 100 * 1000)) % 100 < 10)? "0" : "")	// Add 0 if Hours is less than 10
		<< (time / (100 * 100 * 1000)) % 100 << ":"	//	Hours
		<< (((time / (100 * 1000)) % 100 < 10) ? "0" : "")		// Add 0 if Minutes is less than 10
		<< (time / (100 * 1000)) % 100; 			//	Minutes
	if (scoring)
	{
		std::cout << ":"
			<< (((time / 1000) % 100 < 10) ? "0" : "")			//	Add 0 if Seconds less than 10
			<< (time / 1000) % 100 << ":"			//	Seconds
			<< ((time % 1000 < 100) ? "0" : "")					//	Add 0 if Thousands less than 100
			<< ((time % 1000 < 10) ? "0" : "")					//	Add 0 if Thousands less than 10
			<< time % 1000;							//	Thousandths
	}
	std::cout << std::endl;						//	end line		
}