#include "Medals.h"				//	Medals
#include "Nations.h"			//	Nations
#include "Nation.h"				//	Nation

#include <iostream>				//  cin, cout
#include <fstream>				//	Read/Write File

extern Nations* nations;		//	Global class for Nations

                
Medals::Medals() {	//  Constructor
    for (int i = 0; i < 200; i++) // For loop that empties the arrays
	{
        gold[i] = 0;			//	Set gold medals to 0
        silver[i] = 0;			//	Set silver medals to 0
        bronze[i] = 0;			//	Set bronze medals to 0
    }
}
                
void Medals::display() {	//  Function for displaying medals
	if (numberOfNations > 0)
	{
		std::cout << "\nMEDALS:\n";
		for (int i = 0; i < numberOfNations; i++)      //  Goes through all nations with medals
		{
			//  Shows name of nation, and number of each medal
			std::cout << "\t" << statNation[i]->getName() << std::endl;
			std::cout << "\t\tGold:   " << gold[i] << std::endl;
			std::cout << "\t\tSilver: " << silver[i] << std::endl;
			std::cout << "\t\tBronze: " << bronze[i] << std::endl << std::endl;
		}
	}
	else
		std::cout << "There are currently no registered Nations with any Medals!" << std::endl;
}

                //  Function for adding medals to nations
void Medals::addMedals(const int m[3], std::string name) {  //  Medal and nation name for paramters
	bool added = false;                             //  Bool if added medals
	int i = 0;										//  Counting int

	for (; i < numberOfNations; i++)				//  Goes through all nations currently in array
	{             
		if (statNation[i]->getText() == name)		//  Checks if nations name is equal to parameter
		{     
			gold[i] += m[0];                        //  If true adds medal
			silver[i] += m[1];                      //  {1, 0, 0} for gold, {0, 1, 0} for silver
			bronze[i] += m[2];                      //  {0, 0, 1} for bronze
			added = true;                           //  Sets added to true        
		}
	}

	if (!added)										//  If no medals were added
	{
		addNation(i, name);                         //  Adds nation to list at place i
		gold[i] += m[0];                            //  Adds medals for that nation
		silver[i] += m[1];                          //  {1, 0, 0} for gold, {0, 1, 0} for silver
		bronze[i] += m[2];                          //  {0, 0, 1} for bronze
	}

	sort();                                         //  Sorts the arrays
	writeToFile();									//	Write changes to file
}

void Medals::removeMedals(const int m[3], std::string name) { //  Function for removing medals from array, medal and name of nation for paramters

    for (int i = 0; i < numberOfNations; i++)		//  Goes through all nations currently in array
    {
        if (statNation[i]->getText() == name)		//  Checks if nations name is equal to parameter
        {
            gold[i] -= m[0];                        //  If true removes medals
            silver[i] -= m[1];                      //  {1, 0, 0} for gold, {0, 1, 0} for silver
            bronze[i] -= m[2];                      //  {0, 0, 1} for bronze     
        }
    }
    sort();                                         //  Sorts the arrays
    writeToFile();									//	Write changes to file
}
      
void Medals::sort() { //  Function for sorting arrays
	int temp[3];                                    //  Temp medal array
	char* tempName = nullptr;                       //  Temp name pointer
    Nation* tempNation = nullptr;                   //  Temp nation pointer

	for (int i = 0; i < numberOfNations - 1; i++)  //  Goes through all nations currently in array
	{
		if (gold[i] < gold[i + 1])                 //  If number of gold medals is less than the next
		{
			temp[0] = gold[i];                      //  Sets temp gold medals
			temp[1] = silver[i];                    //  Sets temp silver medals
			temp[2] = bronze[i];                    //  Sets temp bronze medals
			tempName = abriviation[i];              //  Sets temp name
			tempNation = statNation[i];             //  Sets temp nation pointer

			gold[i] = gold[i + 1];                  //  Gets gold medals from next in array
			silver[i] = silver[i + 1];              //  Gets silver medals from next in array
			bronze[i] = bronze[i + 1];              //  Gets bronze medals from next in array
			abriviation[i] = abriviation[i + 1];    //  Gets name from next in array
			statNation[i] = statNation[i + 1];      //  Gets nation pointer from next in array

			gold[i + 1] = temp[0];                  //  Sets gold medals of next in array
			silver[i + 1] = temp[1];                //  Sets silver medals of next in array
			bronze[i + 1] = temp[2];                //  Sets bronze medals of next in array
			abriviation[i + 1] = tempName;          //  Sets name of next in array
			statNation[i + 1] = tempNation;         //  Sets nation pointer of next in array
		}
	}
}
         
void Medals::addNation(int i, std::string name) { //  Function for adding a new nation to medals array                                                
	statNation.push_back(nations->getNation(name)); //  Adds nation to the back of the vector
	abriviation[i] = statNation[i]->getText();      //  Sets abriviation to nations's abriviation
	numberOfNations++;                              //  Increases counter of nations
}


void Medals::writeToFile() {						//	Write all data to file
	std::ofstream outFile("Medals.dta");						//	Output file
	if (outFile)
	{
		outFile << numberOfNations << std::endl;				//	Write number of nations to file
		for (int i = 0; i < numberOfNations; i++)				//	For each nation
		{
			outFile << statNation[i]->getText() << std::endl;	//	Write name to file
			outFile << gold[i] << std::endl;					//	Write gold to file
			outFile << silver[i] << std::endl;					//	Write silver to file
			outFile << bronze[i] << std::endl;					//	Write bronze to file
		}
	}
}

void Medals::readFromFile() {						//	Read all data from file
	std::ifstream inFile("Medals.dta");						//	Input file
	if (inFile)												//	if file exists
	{
		int nrNations;										//	Temp int for number of Nations
		inFile >> nrNations;								//	Read from file
		for (int i = 0; i < nrNations; i++)					//	For each nation
		{
			std::string nation;								//	Nation name
			inFile >> nation;								//	Read name from file
			addNation(i, nation);							//	Set pointer to Nation
			inFile >> gold[i];								//	Read gold from file
			inFile >> silver[i];							//	Read silver from file
			inFile >> bronze[i];							//	Read bronze from file
		}
	}
	else													//	if file doesn't exist
		std::cout << "Couldn't find 'Medals.dta'!" << std::endl;	//	Error message
}