#include "Participants.h"		//	Functions
#include "Globals.h"			//  Globals
#include "Const.h"				//	Consts
#include "Functions.h"			//  Non-class functions
#include "Participant.h"		//	Participant Class

#include <iostream>				//  cin, cout
#include <fstream>				//	ifstream, ofstream


Participants::Participants() {	//	Constructor
	participants = new List(Sorted);	//	List of Participants
}

void Participants::subMenu() {
	printMenu();					//  Shows participant menu
	command = read("\nCommand (Participant):   ");	//  User input

	while (command != 'B')			//  While not 'B' for Back
	{
		switch (command)			//  Menu switch
		{
		case 'N':	newParticipant();	break;	//  Case for new participant
		case 'E':	editParticipant();	break;	//  Case for edit participant
		case 'A':	displayAll();		break;	//  Case for display all
		case 'D':	displayOne();		break;	//  Case for display one		
		case 'H':	subMenuHelp();		break;	//	Gives additional info about each menu choice
		default:	printMenu();		break;	//  Default when none of the above, Shows participant menu
		}
		command = read("\nCommand (Participant):   ");	//  User input
	}
	mainMenu();						//	Display main menu when quitting
}

void Participants::printMenu() {			//  Outputs available commands to window
	std::cout << "\nSUBMENU - PARTICIPANTS"
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - New Participant"
		<< "\n\tE - Edit Participant"
		<< "\n\tA - Display data for all Participants"
		<< "\n\tD - Display all data for one Participant"
		<< "\n\tH - Help"
		<< "\n\tB - Back to Main Menu\n";
}

void Participants::subMenuHelp() {			//	Gives additional info about each menu choice
	std::cout << "\nHELP - PARTICIPANTS"
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - Creates a new Participant with various data"
		<< "\n\tE - Edits an existing Participant"
		<< "\n\tA - Displays data for all Participants"
		<< "\n\tD - Displays all data for one Participant"
		<< "\n\tB - Back to Main Menu\n";
}

void Participants::newParticipant() {						//	Creates new Participant
	if (participants->noOfElements() < MAXPARTICIPANTS)
	{
		int nr = read("Enter ID number for participant", 1, MAXID);	//	ID for new participant
		if (!participants->inList(nr))								//	If ID doesn't exist
		{
			Participant* p = new Participant(nr);					//	Create new participant
			participants->add(p);									//	Add to list
			writeToFile();											//	Update file
		}
		else														//	If ID does exist
			std::cout << "ID already exists." << std::endl;			//	Write error message
	}
	else														
		std::cout << "There's no more room for more Participants!" << std::endl;	//	Write error message
}

void Participants::editParticipant() {						//	Edit a Participant
	if (!participants->isEmpty())
	{
		int nr = read("Enter ID number for participant", 1, MAXID);	//	ID for participant
		if (participants->inList(nr))								//	If ID does exist
		{
			participants->destroy(nr);								//	Destroy previous Participant
			Participant* p = new Participant(nr);					//	Create new Participant with same ID
			participants->add(p);									//	Add to list
			writeToFile();											//	Update file
		}
		else														//	If ID doesn't exist
			std::cout << "ID doesn't exist." << std::endl;			//	Write error message
	}
	else														
		std::cout << "There are no registered Participants!" << std::endl;			//	Write error message
}

void Participants::displayAll() {							//	Display all Participants
	if (!participants->isEmpty())
		participants->displayList();								//	Displays all Participants in list
	else
		std::cout << "There are no registered Participants!" << std::endl;			//	Write error message
}

void Participants::displayOne() {							//	Display one given Participant
	if (!participants->isEmpty())
	{
		std::string temp = readString("Enter ID (1-" + std::to_string(MAXID) + ") or name of Participant: ");
		if (strspn(temp.c_str(), "0123456789") == temp.size())			//	if temp is a number
		{
			int nr = atoi(temp.c_str());								//	ID for Participant
			if (participants->inList(nr))								//	if ID exists
				participants->displayElement(nr);						//	Display Participant
			else														//	if ID doesn't exist
				std::cout << "ID doesn't exist." << std::endl;			//	Write error message
		}
		else															//	if temp is a string
		{
			Participant* participant;									//	Temp Participant pointer
			bool found = false;											//	Whether the program found a matching name
			for (int i = 1; i <= participants->noOfElements(); i++)		//	For each Participant
			{
				participant = (Participant*)participants->removeNo(i);	//	Remove from list
				participants->add(participant);							//	Add back to list
				if (participant->getName() == temp)						//	if name matches
				{
					participant->display();								//	Display element
					found = true;										//	Set found to true
				}
			}
			if (!found)													//	if program didn't find a matching name
				std::cout << "Could not find Participant '" << temp << "'!" << std::endl;	//	Write error message
		}
	}
	else
		std::cout << "There are no registered Participants!" << std::endl;			//	Write error message
}

void Participants::writeToFile() {							//	Write all data to file
	std::ofstream outFile("Participants.dta");					//	Output file
	if (outFile)
	{
		Participant* temp;										//	Temp pointer
		for (int i = 1; i <= participants->noOfElements(); i++)	//	For each participant
		{
			temp = (Participant*)participants->removeNo(i);		//	Remove from list and set temp
			participants->add(temp);							//	Add back into list
			temp->writeToFile(outFile);							//	Write data to file
		}
	}
}

void Participants::readFromFile() {				//	Read all data from file
	std::ifstream inFile("Participants.dta");			//	Input file
	if (inFile)											//	if file exists
	{
		while (!inFile.eof())							//	As long as the program hasn't reached end of file
		{
			int i;										//	Point Type
			inFile >> i; inFile.ignore();			
			if (!inFile.good())							//	If read data wasn't valid
				break;									//	Stop reading
			Participant* p = new Participant(i, inFile);//	Create new Category
			participants->add(p);						//	Add to list
		}
	}
	else												//	if file doesn't exist
		std::cout << "Couldn't find 'Participants.dta'!" << std::endl;	//	Error message
}
                 
void Participants::displayParticipant(int nr) {		//  Function for displaying participant's name
	Participant* temp = (Participant*)participants->remove(nr); //  Gets participant from list
	participants->add(temp);                                    //  Adds participant back to list
	std::cout << temp->getName();                               //  Displays name
}
                    
bool Participants::participantInList(int n) {	//  Function for checking if participant exist
	if (participants->inList(n))					//  Checks if participant is in list
		return true;                                //  Returns true if participant exist
	else											//  Else
		return false;                               //  Returns false if not exist
}
                 
Participant* Participants::getParticipant(int n) {	//  Function for getting a pointer to participant
	Participant* temp = nullptr;                    //  Temp participant pointer
	if (participants->inList(n))					//  Checks if participant is in list
	{                  
		temp = (Participant *) participants->remove(n); //  Sets temp to the participant
		participants->add(temp);                    //  Adds participant back in list
	}
	return temp;                                    //  Returns participant or nullptr
}

int Participants::getNrOfPart() {		//	Returns the number of Participants
	return participants->noOfElements();
}