#pragma once
#include <string>

class Category;				//	predeclaration of catergory
class Exercise;				//	predeclartion of exercise

class ParticipantList {									//	Class for Participant List functions
public:
	void printSubMenu(std::string s);						//	declaration of class functions
	void subMenu(Category* category, Exercise* exercise);	//	Submenu with functions
	void subMenuHelp(std::string s);						//	Displays more detailed instructions for each option
	void newList(std::string s);							//	Creates a new list
	void displayList(std::string s);						//	Displays currnet list
	void removeList(std::string s);							//	Removes List
	void editList(std::string s);							//	Edits list
};