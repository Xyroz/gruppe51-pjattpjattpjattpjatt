#pragma once
#include "ListTool2B.h"						//  List tool

#include <string>							//	String

class Category;								//	Category class

class Categories //	Class for List of Categories and functions for it
{							
private:
	List* categories;						//  List of categories
public:	
	Categories();							//  Constructor
	void printMenu();						//  Display menu choices
	void subMenu();							//  Menu function
	void subMenuHelp();						//	Gives additional info about each menu choice
	void addNewCategory();					//  Add new category to list
	void editCategory();					//  Edit a category from list
	void display();							//  Display main data all categories
	void displayCategory();					//  Display data of spesific category
	Category* Categories::findCategory(std::string name);	//	Finds and returns a Category if it exists
	void writeToFile();						//	Write all data to file
	void readFromFile();					//	Read all data from file
};