#include "Nations.h"	//	
#include "Functions.h"	//	Functions
#include "Globals.h"	//	Globals
#include "Nation.h"		//	Nation
#include "Const.h"		//	Consts

#include <fstream>		//	Read/Write File
#include <iostream>		//	cout, cin

Nations::Nations() {	// Constructor
	nations = new List(Sorted);		// makes a new list of type sorted
}

void Nations::printMenu() {			//  Outputs available commands to window
	std::cout << "\nSUBMENU - NATIONS"
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - New Nation"
		<< "\n\tE - Edit Nation"
		<< "\n\tA - Display data for all Nations"
		<< "\n\tP - Display participants in a given Nation"
		<< "\n\tD - Display all data for a given Nation"
		<< "\n\tH - Help"
		<< "\n\tB - Back to Main Menu\n";
}

void Nations::subMenuHelp() {		//	Gives additional info about each menu choice
	std::cout << "\nHELP - NATIONS"
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - Creates a new Nation with various data"
		<< "\n\tE - Edits various info for an existing Nation"
		<< "\n\tA - Displays data for all Nations"
		<< "\n\tP - Displays all participants in a given Nation"
		<< "\n\tD - Displays all data for a given Nation"
		<< "\n\tB - Back to Main Menu\n";
}

void Nations::subMenu()	{		//	Submenu for choosing options
	printMenu();					//  Shows Nations menu
	command = read("\nCommand (Nations):   ");	//  User input

	while (command != 'B')			//  While not 'B' for Back
	{
		switch (command)			//  Menu switch
		{
		case 'N':	newNation();			break;	//  Case for new Nation
		case 'E':	editNation();			break;	//  Case for edit nation
		case 'A':	displayNations();		break;	//  Case for display all nations
		case 'P':	displayParticipants();	break;	//	Case for displaying a given nations participants
		case 'D':	display();				break;	//	Display all data about a given nation
		case 'H':	subMenuHelp();			break;	//	Gives additional info about each menu choice
		default:	printMenu();			break;	//  Default when none of the above, Shows options menu
		}
		command = read("\nCommand (Nations):   ");	//  User input
	}
	mainMenu();						//	Display main menu when quitting
}

Nation* Nations::newNation() {		//	Creates a new Nation by entering a string and returns a pointer
	char* chr = readNation();			//	Read Nation abriviation
	return newNation(chr);				//	Attempts to create new Nation with char*
}

Nation* Nations::newNation(char* chr) {		//	Creates a new Nation and returns a pointer
	if (nations->noOfElements() < MAXNATIONS)
	{
		Nation* nation = nullptr;			//	Temp pointer
		if (!nations->inList(chr))			//	if Nation isn't in list
		{
			nation = new Nation(chr);		//	Create new Nation
			nations->add(nation);			//	Add the new nation into the list
			writeToFile();					//	Write changes to file
			return nation;					//	Return pointer
		}
		else								//	if Nation already exists
		{
			std::cout << "Nation '" << chr << "' already exists" << std::endl;	//	Error Message
			delete[] chr;
		}
	}
	else
		std::cout << "There's no room for more Nations!" << std::endl;	//	Error Message
}

void Nations::editNation() {		//	Edit a Nation
	if (!nations->isEmpty())
	{
		char* chr = readNation();			//	Read Nation abriviation
		if (nations->inList(chr))			//	if Nation is in list
		{
			Nation* nation = (Nation*)nations->remove(chr);	//	Remove from list
			nation->edit();					//	Edit data in Nation
			nations->add(nation);			//	Add Nation back into list

			writeToFile();					//	Write changes to file
		}
		else								//	if Nation doesn't exist
			std::cout << "Nation '" << chr << "' was not found" << std::endl;	//	Error Message

		delete[] chr;
	}
	else
		std::cout << "There are no registered Nations!" << std::endl;	//	Error Message
}

void Nations::displayNations() {	//	Run the display function for all Nations
	if (!nations->isEmpty())
		nations->displayList();				//	Display List
	else
		std::cout << "There are no registered Nations!" << std::endl;	//	Error Message
}

void Nations::displayParticipants()	{	// Display a given nations participants
	if (!nations->isEmpty())
	{
		char* chr = readNation();				//	Read Nation abriviation
		if (nations->inList(chr))				//	if Nation is in list
		{
			Nation* nation;						//	Temp Nation pointer
			nation = (Nation*)nations->remove(chr);	//	Remove from list
			nation->displayParticipants();		//	Displays Participants
			nations->add(nation);				//	Add Nation back to list
		}
		else									//	if Nation doesn't exist
			std::cout << "Nation '" << chr << "' was not found" << std::endl;	//	Error Message

		delete[] chr;
	}
	else
		std::cout << "There are no registered Nations!" << std::endl;	//	Error Message
}

void Nations::display() {		//	Display info about a nation as well as participants
	if (!nations->isEmpty())
	{
		char* chr = readNation();			//	Read Nation abriviation
		if (nations->inList(chr))			//	if Nation is in list
		{
			Nation* nation;					//	Temp Nation pointer
			nation = (Nation*)nations->remove(chr);	//	Remove from list
			nation->displayAll();			//	Displays all the info about a nation
			nation->displayParticipants();	//	Displays all participatns for a nation
			nations->add(nation);			//	Add Nation back to list
		}
		else								//	if Nation doesn't exist
			std::cout << "Nation '" << chr << "' was not found" << std::endl;	//	Error Message

		delete[] chr;
	}
	else
		std::cout << "There are no registered Nations!" << std::endl;	//	Error Message
}

bool Nations::nationExists(char* chr, Participant* p) {	//	Participant checks if Nation exists and either adds itself to Nation or creates a new Nation and then adds itself
	Nation* nation;						//	Temp Nation pointer
	if (nations->inList(chr))			//	if Nation is in list
	{
		nation = (Nation*)nations->remove(chr);	//	Remove from list
		nation->addParticipant(p);		//	Adds participant to Nation
		nations->add(nation);			//	Add Nation back to list
		return true;					//	Returns true, nation exists
	}
	std::cout << "Nation doesn't exist, please create a new nation..." << std::endl;
	nation = newNation(chr);		//	Creates new Nation
	nation->addParticipant(p);		//	Adds participant to Nation
	return false;					//	Returns false, nation didn't exist
}

void Nations::writeToFile() {					//	Write all data to file
	Nation* temp;										//	Temporary pointer
	std::ofstream out("Nations.dta", std::ofstream::trunc);					//	Creates and opens ofstream object "out"
	out << nations->noOfElements() << std::endl;		//	Writes the number of nations at the top
	for (int i = 1; i <= nations->noOfElements(); i++)	//	For each Nation
	{
		temp = (Nation*)nations->removeNo(i);			//	Remove Nation from list
		temp->writeToFile(out);							//	Write data to file
		nations->add(temp);								//	Add Nation back to the list
	}
}

void Nations::readFromFile() {						//	Read all data from file
	int noOfNations;										//	noOfNations
	char buffer[MAXTEXT];									//	text
	Nation* temp;											//	temp Nation pointer
	std::ifstream inFile("Nations.dta");					//	Find file
	if (inFile)												//	if file exists
	{
		inFile >> noOfNations;								//	Read number of Nations
		for (int i = 1; i <= noOfNations; i++)				//	For each Nation
		{
			inFile >> buffer; inFile.ignore();				//	Read name abriviation
			temp = new Nation(inFile, buffer);				//	Create new Nation from file
			nations->add(temp);								//	Add Nation to List
		}
	}
	else													//	if file doesn't exist
		std::cout << "Couldn't find 'Nations.dta'!" << std::endl;	//	Error message
}

Nation* Nations::getNation(std::string name)		//	Fetches nation and returns it as a pointer
{
	Nation* temp = nullptr;									//	Nation pointer
	if (nations->inList(name.c_str()))						//	Checks if the nation exists
	{
		temp = (Nation*) nations->remove(name.c_str());		//	Remove from list
		nations->add(temp);									//	Add back into list
	}
	return temp;											//	Returns nation or nullptr
}