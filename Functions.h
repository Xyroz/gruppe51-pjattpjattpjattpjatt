#pragma once
#include <string>

//  User input
char read(std::string s);					//	Read and return one upcast character
int read(std::string s, int min, int max); 	//	Read and return an int
std::string readString(std::string s);		//	Read text string
int readDate(std::string s);					//	Read date as int
int readTime(std::string s, bool scoring);	//	Read time as int
char* readNation();							//	Read and return 3 character Nation abriviation
std::string autoCapitalize(std::string s);	//	Auto Capitalizes and removes spaces

//  Main menu
void mainMenu();							//	Prints Main Menu
void menuHelp();							//	Gives additional info about each menu choice
void displayTime(int time, bool scoring);//	Displays formatted time