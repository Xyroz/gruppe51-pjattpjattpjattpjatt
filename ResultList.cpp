#include "ResultList.h"		//	
#include "Exercise.h"		//	Exercise
#include "Functions.h"		//	Standard Functions
#include "Globals.h"		//	Globals
#include "Participants.h"	//	Participants
#include "Placement.h"		//	Placement
#include "Const.h"			//	Consts
#include "Medals.h"			//	Medals
#include "Points.h"			//	Points
#include "Category.h"		//	Category

#include <iostream>			//	cin, cout
#include <fstream>			//	Read/Write File

extern Medals* medals;		//	Global class for Medals
extern Points* points;		//	Global class for Points
extern Participants* participants;	//	Global class for Participants


void ResultList::subMenu(Category* category, Exercise* exercise) {	//	Submenu with various choices regarding Results
	printSubMenu(exercise->getName());				//	Displays submenu
	command = read("\nCommand (Result list):  ");	//	Reads command
	while (command != 'B')							//  While not 'B' for Back
	{
		switch (command)							//  Results switch
		{
		case 'D':	displayList(category, exercise->getName());	break;	//	Display Results
		case 'N':	newList(category, exercise->getName());		break;	//	Create new Result List
		case 'R':	removeList(category->getType(), exercise->getName());			break;	//	Remove Result List
		case 'H':	subMenuHelp(exercise->getName());			break;	//	Print detailed info about each menu choice
		default:	printSubMenu(exercise->getName());			break;	//  Default when none of the above, shows menu
		}
		command = read("\nCommand (Result list):  ");	// Reads command
	}
}

void ResultList::printSubMenu(std::string s) {	//	Prints the various options for the submenu
	std::cout << "\nSUBMENU - RESULTLIST, " << s << ": "
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tD - Display result list"
		<< "\n\tN - New result list"
		<< "\n\tR - Remove result list"
		<< "\n\tH - Help"
		<< "\n\tB - Back to previous sub-menu\n";
}

void ResultList::subMenuHelp(std::string s) {	//	Gives a detailed explanation of each function in the submenu
	std::cout << "\nHELP - RESULTLIST, " << s << ": "
		<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tD - Display result list for Exercise " << s
		<< "\n\tN - Create a new result list for Exercise " << s
		<< "\n\tR - Remove an existing result list for Exercise " << s
		<< "\n\tB - Back to previous sub-menu\n";
}

                        //  Function for displaying result list
void ResultList::displayList(Category* cat, std::string s) {// Category and name as paramaters
	std::ifstream inFile("EX" + s + ".res");                //  Gets result list for reading

	if (inFile)												//  If result list
	{
		std::vector<int> result;                                //  Vector for results
		std::vector<int> vecParticipant;                        //  Vector for ID of participants

		int numParticipants;                                    //  Number of participants
		List* finalResults = new List(Sorted);                  //  Creates a new list

		inFile >> numParticipants;                          //  Reads number of participants
		inFile.ignore();                                    //  Ignores '\0'

		result.resize(numParticipants);                     //  Resizes vector to size number of participants
		vecParticipant.resize(numParticipants);             //  Resizes vector to size number of participants

		for (int i = 0; i < numParticipants; i++)			//  Goes through all participants
		{
			inFile >> result[i];                            //  Reads in result to vector
			inFile.ignore();                                //  Ignores '\0'
			inFile >> vecParticipant[i];                    //  Reads in participant ID to vector
			inFile.ignore();                                //  Ignores '\0'
                                                 
			finalResults->add(new Placement(result[i], vecParticipant[i]));	//  Adds participant and result to list
		}

		if (cat->getType())									//  If category result is time
		{
			for (int i = 1; i <= numParticipants; i++)	//  Goes through all participants (from low to high)
			{                                               //  Gets place 
				Placement* temp = (Placement*)finalResults->removeNo(i);
				finalResults->add(temp);                    //  Adds back to list
				temp->display(cat);                         //  Display results
			}
		}
		else												//  Else category result is points
		{
			for (int i = numParticipants; i > 0; i--)		//  Goes through all participants (from high to low)
			{                                               //  Gets place
				Placement* temp = (Placement*)finalResults->removeNo(i);
				finalResults->add(temp);                    //  Adds back to list
				temp->display(cat);                         //  Display results
			}
		}

		delete finalResults;                                    //  Delete list from memory
	}
    else                                                  
        std::cout << "\nResultlist does not exist!\n"; //  If no result list  
}


                    //  Function for making a new result list
void ResultList::newList(Category* cat, std::string s) {    //  Category and exercise name as parameter
	std::ifstream inFile("EX" + s + ".res");                //  Tries finding the result list

	if (!inFile)	                                        //  If it doesn't exist do:
	{
		std::ifstream partList("EX" + s + ".sta");          //  Tries finding participant list

		if (partList)										//  If participant list
		{
			std::ofstream outFile("EX" + s + ".res");       //  Makes result list

			int numParticipants;                            //  Number of participants
			std::vector<int> vecParticipants;               //  Vector for ID of participants
			std::vector<int> results;                       //  Vector for result

			std::string name;                               //  Temp string name
			std::string nation;                             //  Temp string nation
			int startNum;                                   //  Temp int startnumber

			Placement* placement;                           //  Placement pointer

			partList >> numParticipants;                    //  Gets number of participants
			partList.ignore();                              //  Ignores a '\0'
			vecParticipants.resize(numParticipants);        //  Resize vector to size number of participants
			results.resize(numParticipants);                //  Resize vector to size number of participants

			List* finalResults = new List(Sorted);          //  Makes a new finalresult list

			outFile << numParticipants << std::endl;        //  Writes number of participants to result file


			for (int i = 0; i < numParticipants; i++)		//  For loop to get all participants read
			{
				partList >> startNum;                       //  Read startnumber
				partList.ignore();                          //  Ignores a '\0'
				partList >> vecParticipants[i];             //  Reads participant ID into slot i of vector
				partList.ignore();                          //  Ignores a '\0'
				partList >> name;                           //  Reads participant name
				partList.ignore();                          //  Ignores a '\0'
				partList >> nation;                         //  Reads participant nation
				partList.ignore();                          //  Ignores a '\0'
                
				std::cout << "\nInput result of ";          //  Asks for result for a participant
				participants->displayParticipant(vecParticipants[i]);   //  Displays name of participant
				std::cout << ":  ";

				if (cat->getType())							//  If category result is time:
					results[i] = readTime("\nTime:  ", cat->getType());  //  Asks for time 
				else										//  Else
					results[i] = read("\nPoints:  ", MINPOINTS, MAXPOINTS); //  Asks for points
                                                            
				finalResults->add(placement = new Placement(results[i], vecParticipants[i]));	//  Adds results into a list for sorting
			}

			if (cat->getType())								//  If category result is time
			{
				for (int i = numParticipants; i > 0; i--)	//  Goes through all participants in exercise (low to high)
				{											//  Gets the participant 
					placement = (Placement*)finalResults->removeNo(i);
					finalResults->add(placement);           //  Adds it back to the list
					placement->writeToFile(outFile);        //  Writes to file

					switch (i)								//  Switch for medal and placement
					{
					case 1:									//  Case for last person in the list (first place)
					{
						medals->addMedals(GOLD, placement->getNation());    //  Gives gold medal to the nation
						points->addPoints(FIRST, placement->getNation());   //  Gives points according to placement, to the nation
						break;
					}
					case 2:									//  Case for second place
					{
						medals->addMedals(SILVER, placement->getNation());  //  Gives silver medal to the nation
						points->addPoints(SECOND, placement->getNation());  //  Gives points according to placement, to the nation
						break;
					}
					case 3:									//  Case for third place
					{
						medals->addMedals(BRONZE, placement->getNation());  //  Gives bronze medal to the nation
						points->addPoints(THIRD, placement->getNation());   //  Gives points according to placement, to the nation
						break;
					}
					case 4:									//  Case for fourth place
					{
						points->addPoints(FOURTH, placement->getNation());  //  Gives points according to placement, to the nation
						break;
					}
					case 5:									//  Case for fifth place
					{
						points->addPoints(FIFTH, placement->getNation());   //  Gives points according to placement, to the nation
						break;
					}
					case 6:									//  Case for sixth place
					{
						points->addPoints(SIXTH, placement->getNation());   //  Gives points according to placement, to the nation
						break;
					}
					}

				}
			}
			else											//  Else if result is points
			{
				for (int i = 0; i < numParticipants; i++)	//  Goes through all participants in exercise (high to low)
				{                                           //  Gets participant
					placement = (Placement*)finalResults->removeNo(numParticipants - i);
					finalResults->add(placement);           //  Adds participant back into list
					placement->writeToFile(outFile);        //  Writes result of participant

					switch (i)								//  Switch for placement and medals
					{
					case 0:									//  Case for first place
					{
						medals->addMedals(GOLD, placement->getNation());    //  Gives gold medal to the nation
						points->addPoints(FIRST, placement->getNation());   //  Gives points according to placement, to the nation
						break;
					}
					case 1:									//  Case for second place
					{
						medals->addMedals(SILVER, placement->getNation());  //  Gives silver medal to the nation
						points->addPoints(SECOND, placement->getNation());  //  Gives points according to placement, to the nation
						break;
					}
					case 2:									//  Case for third place        
					{
						medals->addMedals(BRONZE, placement->getNation());  //  Gives bronze medal to the nation
						points->addPoints(THIRD, placement->getNation());   //  Gives points according to placement, to the nation
						break;
					}
					case 3:									//  Case for fourth place
					{
						points->addPoints(FOURTH, placement->getNation());  //  Gives points according to placement, to the nation
						break;
					}
					case 4:									//  Case for fifth place
					{
						points->addPoints(FIFTH, placement->getNation());   //  Gives points according to placement, to the nation
						break;
					}
					case 5:									//  Case for sixth place
					{
						points->addPoints(SIXTH, placement->getNation());   //  Gives points according to placement, to the nation
						break;
					}
					}

				}
			}
			delete finalResults;                            //  Deletes list from memory

		}
        else												//  If participant list does not exist
            std::cout << "\nParticipant list does not exist!\n";
	}
    else													//  If result list already exist
        std::cout << "\nResult list already exist!\n";
}

                    //  Function for removing resultlist
void ResultList::removeList(bool type, std::string s) { 
	std::string fileName("EX" + s + ".res");                //  Creates the file name

    std::ifstream inFile(fileName);                         //  Creates input file

    if (inFile) {                                           //  If file exists

        Placement* placement;                               //  Temporary placement pointer

        std::vector<int> result;                            //  Vector for results
        std::vector<int> vecParticipant;                    //  Vector for ID of participants

        int numParticipants;                                //  Number of participants
        List* finalResults = new List(Sorted);              //  Creates a new list

        inFile >> numParticipants;                          //  Reads number of participants
        inFile.ignore();                                    //  Ignores '\0'

        result.resize(numParticipants);                     //  Resizes vector to size number of participants
        vecParticipant.resize(numParticipants);             //  Resizes vector to size number of participants

        for (int i = 0; i < numParticipants; i++)			//  Goes through all participants
        {
            inFile >> result[i];                            //  Reads in result to vector
            inFile.ignore();                                //  Ignores '\0'
            inFile >> vecParticipant[i];                    //  Reads in participant ID to vector
            inFile.ignore();                                //  Ignores '\0'

            finalResults->add(new Placement(result[i], vecParticipant[i]));	//  Adds participant and result to list
        }

        if (type)								//  If category result is time
        {
            for (int i = numParticipants; i > 0; i--)	//  Goes through all participants in exercise (low to high)
            {											//  Gets the participant 
                placement = (Placement*)finalResults->removeNo(i);
                finalResults->add(placement);           //  Adds it back to the list

                switch (i)								//  Switch for medal and placement
                {
                case 1:									//  Case for last person in the list (first place)
                {
                    medals->removeMedals(GOLD, placement->getNation());    //  Removes gold medal to the nation
                    points->removePoints(FIRST, placement->getNation());   //  Removes points according to placement, to the nation
                    break;
                }
                case 2:									//  Case for second place
                {
                    medals->removeMedals(SILVER, placement->getNation());  //  Removes silver medal to the nation
                    points->removePoints(SECOND, placement->getNation());  //  Removes points according to placement, to the nation
                    break;
                }
                case 3:									//  Case for third place
                {
                    medals->removeMedals(BRONZE, placement->getNation());  //  Removes bronze medal to the nation
                    points->removePoints(THIRD, placement->getNation());   //  Removes points according to placement, to the nation
                    break;
                }
                case 4:									//  Case for fourth place
                {
                    points->removePoints(FOURTH, placement->getNation());  //  Removes points according to placement, to the nation
                    break;
                }
                case 5:									//  Case for fifth place
                {
                    points->removePoints(FIFTH, placement->getNation());   //  Removes points according to placement, to the nation
                    break;
                }
                case 6:									//  Case for sixth place
                {
                    points->removePoints(SIXTH, placement->getNation());   //  Removes points according to placement, to the nation
                    break;
                }
                }

            }
        }
        else											//  Else if result is points
        {
            for (int i = 0; i < numParticipants; i++)	//  Goes through all participants in exercise (high to low)
            {                                           //  Gets participant
                placement = (Placement*)finalResults->removeNo(numParticipants - i);
                finalResults->add(placement);           //  Adds participant back into list

                switch (i)								//  Switch for placement and medals
                {
                case 0:									//  Case for first place
                {
                    medals->removeMedals(GOLD, placement->getNation());    //  Removes gold medal to the nation
                    points->removePoints(FIRST, placement->getNation());   //  Removes points according to placement, to the nation
                    break;
                }
                case 1:									//  Case for second place
                {
                    medals->removeMedals(SILVER, placement->getNation());  //  Removes silver medal to the nation
                    points->removePoints(SECOND, placement->getNation());  //  Removes points according to placement, to the nation
                    break;
                }
                case 2:									//  Case for third place        
                {
                    medals->removeMedals(BRONZE, placement->getNation());  //  Removes bronze medal to the nation
                    points->removePoints(THIRD, placement->getNation());   //  Removes points according to placement, to the nation
                    break;
                }
                case 3:									//  Case for fourth place
                {
                    points->removePoints(FOURTH, placement->getNation());  //  Removes points according to placement, to the nation
                    break;
                }
                case 4:									//  Case for fifth place
                {
                    points->removePoints(FIFTH, placement->getNation());   //  Removes points according to placement, to the nation
                    break;
                }
                case 5:									//  Case for sixth place
                {
                    points->removePoints(SIXTH, placement->getNation());   //  Removes points according to placement, to the nation
                    break;
                }
                }

            }
        }

        inFile.close();                                         //  Closes file
    }

                                                            //  Remove returns 1 if error during
                                                            //  delete. c.str() changes string to char array
    if (remove(fileName.c_str()))							//  If not removed 
        std::cout << "\nResult list doesn't exist!\n";      //  Outputs: Does not exist
    else													//  Else
        std::cout << "\nResult list has been deleted!\n";   //  Outputs: Deleted
}
