#include "Nation.h"					//	
#include "Participant.h"			//	Access to participant class
#include "Const.h"					//	Consts
#include "Functions.h"				//	Read functions

#include <iostream>					//	cout, cin
#include <fstream>					//	Read/Write File

Nation::Nation(std::ifstream& in, char* t) : TextElement(t) {	//	Read from file with text element
	char temp[80];									//	temp char[]
	char temp2[80];									//	temp char[]
	in.getline(temp, MAXTEXT);						//	Read name
	name = temp;									//	Set name
	in.getline(temp2, MAXTEXT);						//	Read Contact Person
	contactPerson = temp2;							//	Set Contact Person
	in >> phoneNumber;								//	Read Phone Number
	participants = new List(Sorted);				//	new list of type sorted
}
	
Nation::Nation(char* t) : TextElement (t) {	//	Constructor with text element
	name = readString("Write the full name of the Nation: ");					//	Read Name
	contactPerson = readString("Write the name of the contact person: ");		//	Read Contact Person
	phoneNumber = read("Write the contacts phone number: ", MINPHONE, MAXPHONE);//	Read Phone Number
	participants = new List(Sorted);											//	new list of type sorted
}

void Nation::edit() {
	name = readString("Write the full name of the Nation: ");					//	Read Name
	contactPerson = readString("Write the name of the contact person: ");		//	Read Contact Person
	phoneNumber = read("Write the contacts phone number: ", MINPHONE, MAXPHONE);//	Read Phone Number
}

void Nation::display() {					//	Display function for the nation
	std::cout << "\nNation abriviation: " << text;								//	Display Nation abriviation
	std::cout << "\nNations full name: " << name;								//	Display Name
	std::cout << "\nNumber of participants: " << participants->noOfElements() << std::endl;	//	Display Number of Participants
}

void Nation::displayParticipants() {		//	Display funtion for the participants 
	if (!participants->isEmpty())						//	if List isn't empty
	{
		std::cout << "\nParticipants:\n" << std::endl;	//	"Title" for participants
		participants->displayList();					//	Display entire list
	}
	else												//	if List is empty
		std::cout << "Participant List is empty!" << std::endl;	//	Error message
}

void Nation::addParticipant(Participant* p)	{	//	Add participant to List
	participants->add(p);								//	Adds Participant to list
}

void Nation::removeParticipant(int id) {		//	Removes a Participant from Nation based on ID
	participants->removeNo(id);							//	Removes the Participant from list
}

std::string Nation::getName() {				//	Returns name of Nation
	return name;
}

void Nation::displayAll() {					//	Display all info
	display();														//	Displays basic info
	std::cout << "\nContact person: " << contactPerson;				//	Display Contact Person
	std::cout << "\nContact number: " << phoneNumber<< std::endl;	//	Display Contact Number
}

void Nation::writeToFile(std::ofstream& out) {	//	Write all data to file
	out << text << std::endl;						//	Write text to file
	out << name << std::endl;						//	Write name to file
	out << contactPerson << std::endl;				//	Write Contact Person to file
	out << phoneNumber << std::endl;				//	Write Phone Number to file
}

char* Nation::getText() {	//	Returns the 3-letter abriviation of the Nation
	return text;
}