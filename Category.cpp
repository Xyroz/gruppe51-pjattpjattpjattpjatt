#include "Category.h"	//	Category
#include "Exercise.h"	//	Exercise
#include "Const.h"		//	Consts
#include "Functions.h"	//	Functions
#include "ResultList.h" //  Resultlist

#include <iostream>		//	cout, cin
#include <fstream>		//	Read/Write File
#include <string>       //  strings

extern ResultList* resultList;


Category::Category(char* name) : TextElement(name) {		//  Constructor, sends parameter to parent
	exercises = new List(Sorted);				//	List of exercises
	bool correct = false;						//  Temp bool for checking input
	char chr;									//  Temp character for input
	numExercises = 0;							//  Sets number of exercises to 0
	chr = read("Results messured in (P)oints or (T)ime:   ");	//	Read input for pointType
												//  Reads if result is points or time
	while (!correct)							//  While not correct input
	{
		switch (chr)							//  Switch for input
		{
		case 'P': {							//  Case 'P' for points
			pointType = Point;					//  Sets enum to points
			correct = true;						//  Input is correct
			break;
		}
		case 'T': {							//  Case 'T' for time
			pointType = Time;					//  Sets enum to time
			correct = true;						//  Input is correct
			break;
		}
		default: {							//  Case when input is not correct
			std::cout << "Typed in wrong value\n";
			chr = read("Results messured in (P)oints or (T)ime:   ");
			break;
		}
		}
	}
}

Category::Category(std::ifstream& in, char* name) : TextElement(name) {	//	Read data from file
	int temp;									//	Temp int for PointType
	in >> temp;									//	Read PointType from file
	pointType = ((temp)?Time:Point);			//	Set PointType
	in >> numExercises;							//	Read Number of Exercises
	exercises = new List(Sorted);				//	List of exercises
	for (int i = 1; i <= numExercises; i++)		//	Loop through amount of Exercises
	{
		char name[MAXTEXT];						//	Name of Exercise
		in >> name;								//	Read Name from file
		Exercise* exercise = new Exercise(in, name);	//	Create Exercise
		exercises->add(exercise);				//	Add Exercise to list
	}
}
			
void Category::edit() {					//  Function for chaning category															
	text = _strdup(readString("Enter name of the category:   ").c_str());				
												//  Reads and sets new name
	std::cout << "Name changed. \n\n";			//  Confirmation message
}
				
void Category::display() {					//  Function for displaying main data
	std::cout << "\nName:  " << text << "\n"	//  Shows name of category
		<< "Results messured in:  ";
	if (pointType) std::cout << "time \n";		//  If time shows time
	else std::cout << "points \n";				//  If points shows points
	std::cout << "Number of exercises:  " << numExercises << "\n\n";
												//  Shows number of exercises in array
}
			
void Category::displayAll() {	//  Function for displaying all data
	display();									//  Displays main data of category
	exercises->displayList();					//  Displays exercise data
}

void Category::newExercise() {	//	Create new Exercise
	if (numExercises < MAXEXERCISES)			//	if there's room for more exercises
	{
		std::string name = readString("Enter the name of the exercise: ");	//  The name of the exercise
		if (!exercises->inList(name.c_str()))								//	if exercise doesn't exist
		{
			Exercise* exercise = new Exercise((char*)name.c_str());			//	Create new Exercise
			exercises->add(exercise);										//	Add to list
			numExercises++;													//	Increase counter
		}
		else																//	if exercise doesn't exist
			std::cout << "Exercise " << name << " already exists!" << std::endl;	//	Error message
	}
	else																	//	if max number of exercises
		std::cout << "This category already has the max amount of exercises!" << std::endl;	//	Error message
}

void Category::editExercise() {	//	Edit existing exercise
	if (numExercises)			//	if there are exercise(s)
	{
		std::string name = readString("Enter the name of the exercise to edit: ");	//  The name of the exercise
		if (exercises->inList(name.c_str()))										//	if exercise exists
		{
			Exercise* temp = (Exercise*)exercises->remove(name.c_str());			//	Temp exercise pointer
			exercises->add(temp);													//	Add back into list
			temp->edit();															//	Edit exercise
		}
		else																		//	if Exercise doesn't exist
			std::cout << "Couldn't find " << name << "!" << std::endl;				//	Error Message
	}
	else																			//	if there are no exercises
		std::cout << "There are no registered exercises for this category!" << std::endl;	//	Error message
}

void Category::deleteExercise() {	//	Delete an exercise
	if (numExercises)								//	if there are exercise(s)
	{
		std::string name = readString("Enter the name of the exercise to delete: ");	//  The name of the exercise
		if (exercises->inList(name.c_str()))											//	if exercise exists
		{
            resultList->removeList(pointType, name);                                    //  Removes resultlist
			exercises->destroy(name.c_str());											//	Destroy exercise
			numExercises--;																//	Reduce counter
			std::cout << name << " has been deleted!" << std::endl;						//	Print message
		}
		else																			//	if exercise doesn't exist
			std::cout << "Couldn't find " << name << "!" << std::endl;					//	Error message
	}
	else																				//	if there are no exercises
		std::cout << "There are no registered exercises for this category!" << std::endl;	//	Error message
}

void Category::displayExercises() {		//	Display all exercises
	if (numExercises)						//	if there are any exercises
		exercises->displayList();			//	Display all
	else									//	if there aren't any exercises
		std::cout << "There are no registered exercises for this category!" << std::endl;	//	Error message
}

Exercise* Category::findExercise(std::string name) {	//	Find and returns an Exercise pointer
	Exercise* exercise = nullptr;							//	Set pointer
	if (exercises->inList(name.c_str()))					//	if Exercise in list
	{
		exercise = (Exercise*)exercises->remove(name.c_str());	//	Remove exercise
		exercises->add(exercise);							//	Add back into list
	}
	return exercise;										//	Return pointer
}

bool Category::getType() {		//	Return type as bool
	return pointType;
}

char* Category::getName() {		//	Return name
	return text;
}

void Category::writeToFile(std::ofstream& out) {	//	Write all data to file
	out << text << std::endl;							//	Write Name to file
	out << pointType << std::endl;						//	Write PointType to file
	out << numExercises << std::endl;					//	Write Number of Exercises to file
	Exercise* temp;										//	Temp pointer
	for (int i = 1; i <= exercises->noOfElements(); i++)//	For each Exercise
	{
		temp = (Exercise*)exercises->removeNo(i);		//	Remove from list and add to pointer
		exercises->add(temp);							//	Add back into list
		temp->writeToFile(out);							//	Write Exercise to file
	}
}