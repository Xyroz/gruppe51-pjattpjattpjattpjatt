#pragma once
#include "ListTool2B.h"	//	List

#include <fstream>		//	Read/Write File

class Participant;		//	Participant
class Category;			//	Category

class Placement : public NumElement                 //  Child of numElement
{
private:
	Participant* participant;                       //  Participant pointer
public:
	Placement(int res, int part);                   //  Constructor
	void readFromFile(std::ifstream & in);          //  Read from file
	void writeToFile(std::ofstream & out);          //  Write to file
	void display(Category* cat);                    //  Display
	std::string getNation();                        //  Get name of nation
};