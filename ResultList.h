#pragma once
#include <string>	//	String

class Category;		//	Category
class Exercise;		//	Exercise

class ResultList	//	Class for Result List functions
{
public:
	void subMenu(Category* category, Exercise* exercise);	//	Submenu with choices
	void printSubMenu(std::string s);						//	Displays submenu
	void subMenuHelp(std::string s);						//	Help
	void displayList(Category* cat, std::string s);         //  Display list function
	void newList(Category* cat, std::string s);             //  New list function
	void removeList(bool type, std::string s);          //  Remove list function
};