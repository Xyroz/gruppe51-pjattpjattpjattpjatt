#include "Globals.h"			//  char comman
#include "Functions.h"			//	Standard Functions
#include "Exercises.h"			//	Exercises
#include "Categories.h"			//	Categories
#include "Category.h"			//	Category
#include "ParticipantList.h"	//	Participant List
#include "ResultList.h"			//	Result List

#include <iostream>				//  cin, cout
#include <string>				//	string

extern Categories* categories;								//	Global class for Categories
ParticipantList* participantList = new ParticipantList();	//	Participant List class
ResultList* resultList = new ResultList();					//	Result List class

void Exercises::subMenu() {				//	Submenu with various choices for Exercises
	if (Category* category = categoryExists())			//	if Category was found
	{
		printSubMenu(category);							//  Shows exercise menu
		command = read("\nCommand (Exercises):   ");	//  User input
		

		while (command != 'B')							//  While not 'B' for Back
		{
			switch (command)			//  Main switch
			{
			case 'N': {							//  Case for new Exercise
				category->newExercise();			//	Creates new Exercise
				categories->writeToFile();			//	Writes changes to file
				break;	
			}
			case 'E': {							//  Case for edit Exercise
				category->editExercise();			//	Edits existing exercise
				categories->writeToFile();			//	Writes changes to file
				break;
			}
			case 'D': {							//  Case for delete Exercise
				category->deleteExercise();			//	Delete an existing exercise
				categories->writeToFile();			//	Write changes to file
				break;
			}
			case 'A': {							//  Case for display Exercise
				category->displayExercises();		//	Display all exercises
				break;
			}
			case 'P': {							//  Case for participant List
				if (Exercise* exercise = exerciseExists(category))	//	if Exercise exists
				{
					participantList->subMenu(category, exercise);	//	Submenu for Participant List
					printSubMenu(category);							//	Display submenu when done with Participant List
				}
				break;
			}
			case 'R': {							//  Case for Result List
				if (Exercise* exercise = exerciseExists(category))	//	if Exercise exists
				{
					resultList->subMenu(category, exercise);		//	Submenu for Result List
					printSubMenu(category);							//	Display submenu when done with Result List
				}
				break;
			}
			case 'H':	subMenuHelp(category);	break;	//	Gives additional info about each menu choice
			default:	printSubMenu(category);	break;	//  Default when none of the above, shows exercise menu
			}
			command = read("\nCommand (Exercises):   ");	//  User input
		}
	}
	mainMenu();		//	Displays main menu when quitting
}

void Exercises::printSubMenu(Category* category) {				//  Outputs available commands to window
	std::cout << "\nSUBMENU - EXERCISES, " << category->getName() << ":"
	<<"\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - New Exercise"
		<< "\n\tE - Edit Exercise"
		<< "\n\tD - Delete Exercise"
		<< "\n\tA - Display all Exercises"
		<< "\n\tP - Participant List functions"
		<< "\n\tR - Result List functions"
		<< "\n\tH - Help"
		<< "\n\tB - Back to Main Menu\n";
}

void Exercises::subMenuHelp(Category* category) {				//  Outputs available commands to window
	std::cout << "\nHELP - EXERCISES, " << category->getName() << ":"
	<< "\nFOLLOWING COMMANDS ARE AVAILABLE:"
		<< "\n\tN - Creates a new Exercise with various data"
		<< "\n\tE - Edits an existing Exercise"
		<< "\n\tD - Deletes an existing Exercise"
		<< "\n\tA - Displays all Exercises"
		<< "\n\tP - Functions for list of Participants in a given Exercise"
		<< "\n\tR - Functions for list of Result for a given Exercise"
		<< "\n\tB - Back to Main Menu\n";
}

Category* Exercises::categoryExists() {		//	If the named category exists, return pointer to category
	std::string s = readString("Please enter the name of the category to edit data in: ");	//	Read category name
	Category* temp = categories->findCategory(s);							//	Find Category
	if (temp == nullptr)													//	if Category not found
		std::cout << "Category '" << s << "' was not found!" << std::endl;	//	Error message
	return temp;															//	Return pointer
}

Exercise* Exercises::exerciseExists(Category* category) {	//	If the named Exercise exists, return pointer to Exercise
	std::string s = readString("Please enter the name of the exercise to edit lists for: ");	//	Read Exercise name
	Exercise* temp = category->findExercise(s);								//	Find Exercise
	if (temp == nullptr)													//	if Exercise not found
		std::cout << "Exercise '" << s << "' was not found!" << std::endl;	//	Error message
	return temp;															//	Return pointer
}
