#include "Exercise.h"	//
#include "Functions.h"	//	Standard Functions

#include <string>		//	String
#include <vector>		//	Vector (array)
#include <iostream>		//  cin, cout


Exercise::Exercise(char* name) : TextElement(name) {		//	Class for individual Exercises
	date = readDate("Enter the date of the exercise:");						//	Date of the exercise
	time = readTime("Enter the time of the exercise:", false);				//  Time of day exercise runs
}

Exercise::Exercise(std::ifstream& in, char* name) : TextElement(name) {	//	Read Exercise from file
	in >> date;							//	Read Date from file
	in >> time;							//	Read Time from file
}

Exercise::~Exercise(){

    std::string name = "EX" + std::string(text) + ".sta";
    if (!remove(name.c_str())) {
        std::cout << "Participant list deleted" << std::endl;
    }
    else {
        std::cout << "Error! file could not be found!" << std::endl;
    }
}

void Exercise::display() {					//	Display Exercise data
	std::cout << "Name: " << text << std::endl;	//	Display Name
	std::cout << "Date: " 						//	Display Date
		<< (date/(100*100)) % 10000 << "."			//	Year
		<< (date/100) % 100 << "."					//	Month
		<< date % 100 << std::endl;					//	Day
	displayTime(time, false);					//	Display time
	std::cout << std::endl;						//	Add line
}

void Exercise::edit() {													//	Edit name, date, and time of Exercise
	text = _strdup(readString("Enter new name for the exercise: ").c_str());//  The name of the exercise
	date = readDate("Enter new date for the exercise:");					//	Date of the exercise
	time = readTime("Enter new time for the exercise:", false);				//  Time of day exercise runs
}

char* Exercise::getName() {	//	Returns the Exercise name
	return text;
}

void Exercise::writeToFile(std::ofstream& out) {	//	Write all data to file
	out << text << std::endl;							//	Write Name to file
	out << date << std::endl;							//	Write Date to file
	out << time << std::endl;							//	Write Time to file
}