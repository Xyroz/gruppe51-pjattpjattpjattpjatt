#pragma once
#include "ListTool2B.h"				//  NumElement

class Participant;

class Participants //	Class containing List of Participants and various functions
{				
private:
	List* participants;						//	List of Participants
public:
	Participants();							//	Constructor
	void subMenu();							//	Menu for participants
	void printMenu();						//	Display function for the menu
	void subMenuHelp();						//	Gives additional info about each menu choice
	void newParticipant();					//	Create new participant
	void editParticipant();					//	Edit existing participant
	void displayAll();						//	Display info for all participants
	void displayOne();						//	Display all info for one participant
	void writeToFile();						//	Write all data to file
	void readFromFile();				    //	Read all data from file
	void displayParticipant(int n);         //  Display participant name
	bool participantInList(int n);          //  Participant exist T/F
	Participant* getParticipant(int n);     //  Get pointer to participant
	int getNrOfPart();						//	Reutrns the number of Participants
};
